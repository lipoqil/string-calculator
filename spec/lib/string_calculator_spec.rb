describe StringCalculator do
  include StringCalculator

  describe "add" do
    subject { add(numbers: numbers) }

    context 'with empty input' do
      let(:numbers) { '' }

      it { is_expected.to eq 0 }
    end

    context 'with input "1"' do
      let(:numbers) { '1' }

      it { is_expected.to eq 1 }
    end

    context 'with input "1,2"' do
      let(:numbers) { '1,2' }

      it { is_expected.to eq 3 }
    end

    context 'with input "1,2,3,4"' do
      let(:numbers) { '1,2,3,4' }

      it { is_expected.to eq 10 }
    end

    context 'with input "1\n2,3"' do
      let(:numbers) { "1\n2,3" }

      it { is_expected.to eq 6 }
    end


    context 'with input "1;-2"' do
      let(:numbers) { "//[;]\n1;-2" }

      it 'raises an error on negative input number' do
        expect { subject }.to raise_error('negatives not allowed -- -2')
      end
    end

    context 'with input "2,1001"' do
      let(:numbers) { '2,1001' }

      it { is_expected.to eq 2 }
    end

    describe 'custom delimiters' do
      context 'with custom delimiter ; and input "//[;]\n1;2"' do
        let(:numbers) { "//[;]\n1;2" }

        it { is_expected.to eq 3 }
      end

      context 'with custom delimiter *** and input "//[***]\n1***2***3"' do
        let(:numbers) { "//[***]\n1***2***3" }

        it { is_expected.to eq 6 }
      end

      context 'with custom delimiters *, % and input "//[*][%]\n1*2%3"' do
        let(:numbers) { "//[*][%]\n1*2%3" }

        it { is_expected.to eq 6 }
      end

      context 'with custom delimiters *#, % and input "//[*#][%]\n1*2%3"' do
        let(:numbers) { "//[*#][%]\n1*#2%3" }

        it { is_expected.to eq 6 }
      end
    end
  end
end
