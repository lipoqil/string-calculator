module StringCalculator
  # @param [String] numbers
  def add(numbers:)
    processed_numbers = process_numbers(numbers)

    negatives = processed_numbers.each_with_object([]) do |number, memo|
      number = number.to_i
      memo << number if number.negative?
    end

    raise ArgumentError, "negatives not allowed -- #{negatives.join(', ')}" unless negatives.empty?

    processed_numbers.inject(0) do |memo, input_number |
      if input_number.to_i > 1000
        memo
      else
        memo + input_number.to_i
      end
    end
  end

  private

  # @param [String] numbers
  #
  # @return [Array<String>]
  def process_numbers(numbers)
    if numbers.lines[0]&.start_with?('//')
      separator = numbers.lines[0]&.match(%r{//\[(.*)\]})
      separator = /[#{separator[1].sub('][', '')}]+/
      working_numbers = numbers.lines
      working_numbers.shift
      working_numbers = working_numbers.join("\n")
    else
      working_numbers = numbers
    end
    separator ||= /[,\n]+/
    working_numbers.split(separator)
  end
end
